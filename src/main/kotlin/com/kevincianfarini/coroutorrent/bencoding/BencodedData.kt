package com.kevincianfarini.coroutorrent.bencoding

import okio.ByteString
import okio.ByteString.Companion.toByteString

sealed class BencodedData {

    data class BencodedInt(val value: Int) : BencodedData() {
        override fun toString(): String = value.toString()
    }

    data class BencodedString(val value: ByteString) : BencodedData() {
        constructor(value: String) : this(value.toByteArray().toByteString())

        override fun toString(): String = value.utf8()
    }

    data class BencodedList(val value: List<BencodedData>) : BencodedData() {
        override fun toString(): String = value.toString()
    }

    data class BencodedDict(val value: Map<BencodedString, BencodedData>) : BencodedData() {
        override fun toString(): String = value.toString()

        operator fun get(key: String): BencodedData {
            return requireNotNull(value[BencodedString(key)]) { "$key not in $this" }
        }

        operator fun contains(key: String): Boolean = BencodedString(key) in value
    }

    fun encode(): ByteString = Encoder(this).encode()
    fun asString(): BencodedString = this as BencodedString
    fun asInt(): BencodedInt = this as BencodedInt
    fun asList(): BencodedList = this as BencodedList
    fun asDict(): BencodedDict = this as BencodedDict
}