package com.kevincianfarini.coroutorrent.bencoding

import com.kevincianfarini.coroutorrent.util.contains
import okio.ByteString
import okio.EOFException

class Decoder(private val data: ByteString) {
    private var index: Int = 0

    fun decode(): BencodedData = parseNextDataType()

    private fun peek(): Byte {
        return if (index + 1 > data.size) {
            throw IndexOutOfBoundsException(index)
        } else {
            data[index]
        }
    }

    private fun parseNextDataType(): BencodedData = when (BencoderToken.fromToken(peek())) {
        BencoderToken.INTEGER -> parseInteger()
        BencoderToken.STRING -> parseString()
        BencoderToken.LIST -> parseList()
        BencoderToken.DICT -> parseDict()
        else -> throw IllegalStateException("Tried to parse from ${peek()}")
    }

    private fun read(numBytes: Int = 1): ByteString {
        val readData: ByteString = data.substring(index, index + numBytes)
        index += numBytes
        return readData
    }

    private fun readToNextToken(token: ByteString): ByteString {
        var nextTokenIndex: Int = index

        while (token != data.substring(nextTokenIndex, nextTokenIndex + token.size)) {
            if (nextTokenIndex + token.size > data.size) throw EOFException("Unexpected EOF")
            nextTokenIndex += token.size
        }

        return read(nextTokenIndex - index + 1)
    }

    private fun parseInteger(): BencodedData.BencodedInt {
        val value: Int = readToNextToken(BencoderToken.END.token).run {
            this.substring(1, size - 1).utf8().toInt()
        }

        return BencodedData.BencodedInt(value)
    }

    private fun parseString(): BencodedData.BencodedString {
        val stringSize: Int = readToNextToken(ByteString.of(':'.toByte())).run {
            this.substring(0, size - 1).utf8().toInt()
        }

        return BencodedData.BencodedString(read(stringSize))
    }

    private fun parseList(): BencodedData.BencodedList {
        // throw away. Already `peek` this value to know it's a list
        readToNextToken(BencoderToken.LIST.token)

        val list: MutableList<BencodedData> = mutableListOf()
        while (peek() !in BencoderToken.END.token) { // check to see if we've gotten to the end of the list
            list.add(parseNextDataType())
        }
        read() // read and discard the lists terminator

        return BencodedData.BencodedList(list)
    }

    private fun parseDict(): BencodedData.BencodedDict {
        readToNextToken(BencoderToken.DICT.token) // read and discard

        val map: MutableMap<BencodedData.BencodedString, BencodedData> = mutableMapOf()
        while (peek() !in BencoderToken.END.token) {
            map[parseString()] = parseNextDataType()
        }
        read() // discard dictionary end terminator

        return BencodedData.BencodedDict(map)
    }
}