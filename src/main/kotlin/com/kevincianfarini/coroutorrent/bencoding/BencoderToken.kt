package com.kevincianfarini.coroutorrent.bencoding

import com.kevincianfarini.coroutorrent.util.contains
import okio.ByteString

internal enum class BencoderToken(val token: ByteString) {
    INTEGER(ByteString.of('i'.toByte())),
    STRING(ByteString.of(*"123456789:".toByteArray())),
    LIST(ByteString.of('l'.toByte())),
    DICT(ByteString.of('d'.toByte())),
    END(ByteString.of('e'.toByte()));

    companion object {
        fun fromToken(token: Byte): BencoderToken = when (token) {
            in INTEGER.token -> INTEGER
            in STRING.token -> STRING
            in LIST.token -> LIST
            in DICT.token -> DICT
            in END.token -> END
            else -> throw IllegalArgumentException("Not a BencoderToken")
        }
    }
}

