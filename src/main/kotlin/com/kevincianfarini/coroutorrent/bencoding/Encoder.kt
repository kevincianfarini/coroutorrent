package com.kevincianfarini.coroutorrent.bencoding

import okio.Buffer
import okio.ByteString

class Encoder(private val data: BencodedData) {

    fun encode(): ByteString = encodeValue(data)

    private fun encodeValue(value: BencodedData): ByteString = when (value) {
        is BencodedData.BencodedInt -> encodeInt(value)
        is BencodedData.BencodedString -> encodeString(value)
        is BencodedData.BencodedList -> encodeList(value)
        is BencodedData.BencodedDict -> encodeDict(value)
    }

    private fun encodeInt(value: BencodedData.BencodedInt): ByteString {
        return ByteString.of('i'.toByte(), *value.value.toString().toByteArray(), 'e'.toByte())
    }

    private fun encodeString(value: BencodedData.BencodedString): ByteString {
        return ByteString.of(
                *value.value.size.toString().toByteArray(),
                ':'.toByte(),
                *value.value.toByteArray()
        )
    }

    private fun encodeList(value: BencodedData.BencodedList): ByteString = Buffer().apply {
        write("l".toByteArray())
        value.value.forEach { write(encodeValue(it)) }
        write("e".toByteArray())
    }.snapshot()

    private fun encodeDict(value: BencodedData.BencodedDict): ByteString = Buffer().apply {
        write("d".toByteArray())
        value.value.forEach { (key, value) ->
            write(encodeString(key))
            write(encodeValue(value))
        }
        write("e".toByteArray())
    }.snapshot()
}