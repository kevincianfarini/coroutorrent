package com.kevincianfarini.coroutorrent.torrent

data class TorrentFile(val name: String, val length: Int)