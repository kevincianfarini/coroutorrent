package com.kevincianfarini.coroutorrent.torrent

import com.kevincianfarini.coroutorrent.bencoding.BencodedData
import com.kevincianfarini.coroutorrent.util.decode
import com.kevincianfarini.coroutorrent.util.flatten
import okio.*
import okio.ByteString.Companion.toByteString
import java.io.File

class Torrent(private val torrentFile: File) {

    init {
        require(torrentFile.exists())
    }

    private val meta: BencodedData.BencodedDict by lazy { initMeta() }
    private val metaInfo: BencodedData.BencodedDict by lazy { checkNotNull(meta["info"].asDict()) }
    private val announceUrl: String by lazy { meta["announce"].asString().toString() }
    val announceUrlList: List<String> by lazy { getAnnounceUrls() }
    val infoHash: ByteString by lazy { metaInfo.encode().sha1() }
    val isMultiFile: Boolean by lazy { "files" in metaInfo }
    val files: List<TorrentFile> by lazy { getMetaFiles() }
    val pieceLength: Int by lazy { metaInfo["piece length"].asInt().value }
    val contentLength: Int by lazy { metaInfo["length"].asInt().value }
    val outputName: String by lazy { metaInfo["name"].asString().toString() }
    val hashedPieces: List<ByteString> by lazy { getHashedPieces(metaInfo["pieces"].asString()) }

    constructor(path: String) : this(File(path))

    private fun initMeta(): BencodedData.BencodedDict = Buffer().apply {
        torrentFile.source().buffer().run {
            while (!exhausted()) {
                read(this@apply, 4096)
            }
        }
    }.snapshot().decode().asDict()

    private fun getAnnounceUrls(): List<String> = if ("announce-list" in meta) {
        meta["announce-list"].asList().flatten().map { it.toString() }
    } else {
        listOf(announceUrl)
    }


    private fun getMetaFiles(): List<TorrentFile> = if (isMultiFile) {
        mapMetaFiles(metaInfo["files"].asList())
    } else {
        listOf(TorrentFile(outputName, contentLength))
    }


    private fun mapMetaFiles(metaFiles: BencodedData.BencodedList) = metaFiles.value.map { data ->
        with(data.asDict()) {
            return@map TorrentFile(
                    this["path"].asList().value.joinToString("/"),
                    this["length"].asInt().value
            )
        }
    }

    private fun getHashedPieces(pieces: BencodedData.BencodedString): List<ByteString> {
        val rawData = pieces.value.also {
            require(it.size % 20 == 0) { "piece hashes should be aligned to 20 bytes" }
        }

        return rawData.toByteArray().toList().chunked(20) {
            it.toByteArray().toByteString()
        }
    }
}