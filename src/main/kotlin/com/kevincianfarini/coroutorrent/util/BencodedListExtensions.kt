package com.kevincianfarini.coroutorrent.util

import com.kevincianfarini.coroutorrent.bencoding.BencodedData

fun BencodedData.BencodedList.flatten(): List<BencodedData> {
    val flattenedList: MutableList<BencodedData> = mutableListOf()
    for (item in this.value) {
        when(item) {
            is BencodedData.BencodedList -> flattenedList.addAll(item.flatten())
            else -> {
                flattenedList.add(item)
            }
        }
    }

    return flattenedList
}