package com.kevincianfarini.coroutorrent.util

import com.kevincianfarini.coroutorrent.bencoding.Decoder
import com.kevincianfarini.coroutorrent.bencoding.BencodedData
import okio.ByteString

operator fun ByteString.contains(element: Byte): Boolean {
    return this.indexOf(ByteString.of(element)) != -1
}

fun ByteString.decode(): BencodedData = Decoder(this).decode()