package com.kevincianfarini.coroutorrent.unit.bencoding

import com.kevincianfarini.coroutorrent.bencoding.Decoder
import com.kevincianfarini.coroutorrent.bencoding.BencodedData
import okio.ByteString
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class DecoderTest {

    @Test
    fun testInt() {
        val data: BencodedData = Decoder(ByteString.of(*"i123e".toByteArray())).decode()
        assertTrue(data is BencodedData.BencodedInt)
        assertEquals(123, (data as BencodedData.BencodedInt).value)
    }

    @Test
    fun testString() {
        val data: BencodedData = Decoder(ByteString.of(*"12:Middle Earth".toByteArray())).decode()
        assertTrue(data is BencodedData.BencodedString)
        assertEquals("Middle Earth", (data as BencodedData.BencodedString).value.utf8())
    }

    @Test
    fun testList() {
        val data: BencodedData = Decoder(ByteString.of(*"l4:spam4:eggsi123ee".toByteArray())).decode()
        assertTrue(data is BencodedData.BencodedList)
        assertEquals(
                BencodedData.BencodedList(listOf(
                        BencodedData.BencodedString(ByteString.of(*"spam".toByteArray())),
                        BencodedData.BencodedString(ByteString.of(*"eggs".toByteArray())),
                        BencodedData.BencodedInt(123)
                )),
                data
        )
    }

    @Test
    fun testDict() {
        val data: BencodedData = Decoder(ByteString.of(*"d3:cow3:moo4:spam4:eggse".toByteArray())).decode()
        assertTrue(data is BencodedData.BencodedDict)
        assertEquals(2, (data as BencodedData.BencodedDict).value.size)
    }
}