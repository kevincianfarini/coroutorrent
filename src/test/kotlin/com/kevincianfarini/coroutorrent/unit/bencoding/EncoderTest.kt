package com.kevincianfarini.coroutorrent.unit.bencoding

import com.kevincianfarini.coroutorrent.bencoding.Encoder
import com.kevincianfarini.coroutorrent.bencoding.BencodedData
import okio.ByteString
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class EncoderTest {

    @Test
    fun testInt() {
        val data = Encoder(BencodedData.BencodedInt(123)).encode()
        assertEquals(ByteString.of(*"i123e".toByteArray()), data)
    }

    @Test
    fun testString() {
        val data = Encoder(BencodedData.BencodedString(ByteString.of(*"Middle Earth".toByteArray()))).encode()
        assertEquals(
                ByteString.of(*"12:Middle Earth".toByteArray()),
                data
        )
    }

    @Test
    fun testList() {
        val data = Encoder(BencodedData.BencodedList(listOf(
                BencodedData.BencodedString(ByteString.of(*"spam".toByteArray())),
                BencodedData.BencodedString(ByteString.of(*"eggs".toByteArray())),
                BencodedData.BencodedInt(123)
        ))).encode()

        assertEquals(
                ByteString.of(*"l4:spam4:eggsi123ee".toByteArray()),
                data
        )
    }

    @Test
    fun testDict() {
        val data = Encoder(BencodedData.BencodedDict(mapOf(
                BencodedData.BencodedString(ByteString.of(*"cow".toByteArray())) to BencodedData.BencodedString(ByteString.of(*"moo".toByteArray())),
                BencodedData.BencodedString(ByteString.of(*"spam".toByteArray())) to BencodedData.BencodedString(ByteString.of(*"eggs".toByteArray()))
        ))).encode()

        assertEquals(
                ByteString.of(*"d3:cow3:moo4:spam4:eggse".toByteArray()),
                data
        )
    }
}