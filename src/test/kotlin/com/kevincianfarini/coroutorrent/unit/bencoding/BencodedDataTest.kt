package com.kevincianfarini.coroutorrent.unit.bencoding

import com.kevincianfarini.coroutorrent.bencoding.BencodedData
import com.kevincianfarini.coroutorrent.util.flatten
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class BencodedDataTest {

    @Test
    fun `nested bendcoded lists an be flattened`() {
        val list = BencodedData.BencodedList(listOf(
                BencodedData.BencodedList(listOf(BencodedData.BencodedString("foo"))),
                BencodedData.BencodedList(listOf(BencodedData.BencodedInt(1))),
                BencodedData.BencodedList(listOf(BencodedData.BencodedString("bar")))
        ))

        assertEquals(
                listOf(
                        BencodedData.BencodedString("foo"),
                        BencodedData.BencodedInt(1),
                        BencodedData.BencodedString("bar")
                ),
                list.flatten()
        )
    }

    @Test
    fun `mixed nesting lists can be flattened`() {
        val list = BencodedData.BencodedList(
                listOf(
                        BencodedData.BencodedList(listOf(BencodedData.BencodedString("foo"))),
                        BencodedData.BencodedList(listOf(BencodedData.BencodedInt(1))),
                        BencodedData.BencodedList(listOf(BencodedData.BencodedString("bar"))),
                        BencodedData.BencodedString("baz"),
                        BencodedData.BencodedList(
                                listOf(
                                        BencodedData.BencodedList(
                                                listOf(
                                                        BencodedData.BencodedInt(1),
                                                        BencodedData.BencodedInt(2)
                                                )
                                        )
                                )
                        )
                )
        )

        assertEquals(
                listOf(
                        BencodedData.BencodedString("foo"),
                        BencodedData.BencodedInt(1),
                        BencodedData.BencodedString("bar"),
                        BencodedData.BencodedString("baz"),
                        BencodedData.BencodedInt(1),
                        BencodedData.BencodedInt(2)
                ),
                list.flatten()
        )
    }
}