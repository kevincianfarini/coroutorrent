package com.kevincianfarini.coroutorrent.integration.torrent

import com.kevincianfarini.coroutorrent.torrent.Torrent
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.File

class SingleFileTorrentTest {

    private val ubuntu = Torrent(
            getFileFromResource("ubuntu-18.04.3-desktop-amd64.iso.torrent")
    )

    private val bigBuckBunny = Torrent(
            getFileFromResource("bigbuckbunny_production.torrent")
    )

    @Test
    fun `ubuntu torrent has one file`() {
        assertEquals(1, ubuntu.files.size)
    }

    @Test
    fun `ubuntu torrent is not multi-file`() {
        assertFalse(ubuntu.isMultiFile)
    }

    @Test
    fun `ubuntu torrent output name is correct`() {
        assertEquals(
                "ubuntu-18.04.3-desktop-amd64.iso",
                ubuntu.outputName
        )
    }

    @Test
    fun `ubuntu torrent content length is 2082816000`() {
        assertEquals(2082816000, ubuntu.contentLength)
    }

    @Test
    fun `ubuntu torrent piece length is 524288`() {
        assertEquals(524288, ubuntu.pieceLength)
    }

    @Test
    fun `ubuntu torrent has correct amount of announce URLs`() {
        assertEquals(2, ubuntu.announceUrlList.size)
    }

    @Test
    fun `ubuntu torrent announce URLs are correct`() {
        assertEquals(
                listOf(
                        "https://torrent.ubuntu.com/announce",
                        "https://ipv6.torrent.ubuntu.com/announce"
                ),
                ubuntu.announceUrlList
        )
    }

    @Test
    fun `ubuntu torrent info hash is correct size`() {
        assertEquals(20, ubuntu.infoHash.size)
    }

    @Test
    fun `ubuntu torrent info hash is correct content`() {
        assertEquals(
                "65145ed4d745cfc93f5ffe3492e9cde54b557d9f",
                ubuntu.infoHash.hex()
        )
    }

    @Test
    fun `ubuntu torrent has correct amount of hashed pieces`() {
        assertEquals(3973, ubuntu.hashedPieces.size)
    }

    @Test
    fun `bunny torrent is multi file`() {
        assertTrue(bigBuckBunny.isMultiFile)
    }

    @Test
    fun `bunny torrent has 423 files`() {
        assertEquals(423, bigBuckBunny.files.size)
    }

    @Test
    fun `bunny torrent has 1092 pieces`() {
        assertEquals(1092, bigBuckBunny.hashedPieces.size)
    }

    @Test
    fun `bunny torrent output file is named production`() {
        assertEquals("production", bigBuckBunny.outputName)
    }

    @Test
    fun `bunny torrent has one announce URL`() {
        assertEquals(1, bigBuckBunny.announceUrlList.size)
    }

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    private fun getFileFromResource(fileName: String): File = File(
            javaClass.classLoader.getResource(fileName).file
    )
}
