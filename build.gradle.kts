import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet

group = "com.kevincianfarini.coroutorrent"
version = "0.0.1"

plugins {
    kotlin("jvm") version "1.3.50"
}

sourceSets["main"].withConvention(KotlinSourceSet::class) {
    kotlin.srcDir("src/main/kotlin")
}

sourceSets["test"].withConvention(KotlinSourceSet::class) {
    kotlin.srcDir("src/test/kotlin")
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform()
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2")
    implementation("com.squareup.okio:okio:2.4.1")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
}